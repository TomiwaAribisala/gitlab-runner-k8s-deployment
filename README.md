## Working process of deploying an application to k8s(minikube) cluster using Gitlab

### Prerequisites
- Run a minikube or cloud platform cluster 
- Install `kubectl`on local machine, connect to the cluster with the cluster `kubeconfig`
- export KUBECONFIG=/directory/kubeconfig.yaml
- `kubectl cluster-info`
- `kubectl get namespace`

### Create Gitlab Runner User on K8s Cluster
- Create Gitlab Runner, install `kubectl` on gitlab runner
- Create a namespace in the k8s cluster, `kubectl create namespace [name]`
- Create a dedicated User, a serviceaccount for the gitlab runner, `kubectl create --serviceaccount=[name] --namespace=[name]`
- Create the dedicated user permissions yaml file(Role K8s object), cicd-role.yaml
- On your local machine in the cluster, create the permissions file, `kubectl apply -f cicd-role.yaml`
- Create a RoleBinding object to bind the Role permissions file and the serviceaccount, `kubectl create rolebinding [name] --role=[name] --serviceaccount=[namespace]:[serviceaccount] --namespace=[name]`
- Create a copy on the cluster admin `kubeconfig`, adjust user and token details, grab secret token name details of the serviceaccount--`kubectl get serviceaccount [name] --namespace=[name] -o yaml`, then, `kubectl get secret [token name] --namespace=[name] -o yaml`, and decode the token--`echo [token] | base64 -D`, copy decoded token to the new `kubeconfig` and adjust `username` in the `users` and `contexts` of the kubeconfig, check other details, save and exit
- Add new `kubeconfig` details to the application Gitlab repository as a `variable` in the CI/CD section 

### Deploy to K8s cluster Part 1
- Create k8s manifests(deployment, service, configmap, secret) for the application
- Create a `kubernetes` directory in the application root and place the manifests in the directory
- Write the Gitlab CI/CD pipeline(`deploy job`) to deploy the to the k8s cluster, which will include referencing the `kubeconfig` variable(`export KUBECONFIG=$KUBE_CONFIG`) and the command for the k8s cluster to pull the application image from Gitlab docker registry, `kubectl create secret docker-registry my-registry-key --docker-server=$CI_REGISTRY --docker-username=$GITLAB_USER --docker-password=$GITLAB_PASSWORD --namespace=[name]`, GITLAB_USER and GITLAB_PASSWORD are your Gitlab account username(email) and password details. 
- Include the secret `my-registry-key` in the application deployment manifest 
```
deploy: 
    stage: deploy
    before_script:
        - export KUBECONFIG=$KUBE_CONFIG
    script:
        - kubectl create secret docker-registry my-registry-key --docker-server=$CI_REGISTRY --docker-username=$GITLAB_USER --docker-password=$GITLAB_PASSWORD --namespace=[name]
        - kubectl apply -f kubernetes/deployment.yaml
        - kubectl apply -f kubernetes/service.yaml
```

### Deploy to K8s cluster Part 2
- Run the pipeline
- Check the deployment and service running in the cluster, `kubectl get service --namespace=[name]`, `kubectl get deployment --namespace=[name]`, `kubectl get pod --namespace=[name]`
- Access the service via the cluster LoadBalancer